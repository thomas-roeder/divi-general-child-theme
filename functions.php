<?php
    function my_theme_enqueue_styles() { 
        $prefix = "child-theme-";
		wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
		
		$files= [
			["label" => "general-css-file","path" => "/style.css"],
			["label" => "elements","path" => "/css/elements.css"],
			["label" => "compaire-table","path" => "/css/components/compaire_table/style.css"],
		];

		foreach($files as $file){       
            $file_path = get_stylesheet_directory_uri().$file['path'];
            $version = filemtime(get_theme_file_path().$file['path']);
            $media = 'all';
			wp_enqueue_style($prefix.$file['label'], $file_path, ['parent-style'], $version, $media);

        }        
    }
    add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles', 30 );
?>